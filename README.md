# gefidi
Gesellschafliche Fragen in der Informatik

Hier gibt es eine Menge an Vorträgen und ein Vortragsverwaltungssystem.
Die Vorträge sind mit reveal.js (link) gebaut.
Alle Vorträge sind zu Fortbildungs- und Aufklärungszwecken erstellt.
Außerdem sollen sie Freude und Spaß verbreiten.

## Installation

Zur Installation kann **make.sh** verwendet werden.
Es kann optional mit angegeben werden, welcher Vortrag initial aktiviert werden soll.
```
./make.sh {example}
```

In diesem Repo vorhandene Vortraege sind:
* **auto**: Ein Vortrag zur ethischen Auseinandersetzung mit autonomen Autos
* **diskriminierung**: Ein Vortrag um Probleme durch falsch genutzte Datentypen zu sensibilisieren
* **ki**: Ein Vortrag um aufzuzeigen, welche Probleme es bei Machiene Learning in der Praxis gibt


### Manuelle Installation:

1. Downloade reveal.js
```
git clone https://github.com/hakimel/reveal.js.git
```
2. Ersetze die index.html von reveal.js durch eine .html aus diesem Repo
```
cp {example}.html reveal.js/index.html
```
3. Kopiere die assets
```
# mkdir reveal.js/assets
cp assets/$vortrag/* reveal.js/assets/
```

Jetzt kann die Praesentation genutzt werden, indem **reveal.js/index.html** in einem Browser geoeffnet wird.

### Vortragswechsel

Um einen Vortrag zu aktivieren kann ebenfalls das Script **make.sh** verwendet werden.
Eine Neu-Installation von reveal.js wird nicht durchgeführt.
```
./make {example}
```

Ist bereits ein anderer Vortrag aktiviert, wird dieser einfach ersetzt.
Siehe **Schritt 2 und 3** aus der **Manuellen Installation**.

## Slides

Alle Slides der Praesentation sind in der **index.html** enthalten.
Jede Slide wird durch ein `<section>` repraesentiert.

Fuer Details lies das Readme von reveal.js.
https://github.com/hakimel/reveal.js

## Nutzungsanleitung

Mit den Pfeiltasten kann durch die Praesentation navigiert werden.
Mit Esc kann in eine Slide-overview-Ansicht gewechselt werden.
Mit F kann in den Vollbildmodus gewechselt werden.

Fuer Details schau im Readme von reveal.js nach.
https://github.com/hakimel/reveal.js.git

## Vortraege

* auto
* diskriminierung
* ki

*todo: abstracts zu den vorträgen schreiben*

### auto

Vortragsdauer: 42 Minuten

### diskriminierung

Vortragsdauer: 23 Minuten

### ki

Vortragsdauer: 42 Minuten

### Geplante Vortraege

*todo*

## License

> ----------------------------------------------------------------------------
> "THE BEER-WARE LICENSE" (Revision 42):
> <feconi@posteo.de> wrote this thing. As long as you retain this notice you
> can do whatever you want with this stuff. If we meet some day, and you think
> this stuff is worth it, you can buy me a beer in return - Asterix
> ----------------------------------------------------------------------------


#!/bin/bash

if [ ! -d "reveal.js" ]; then
    echo -e "\e[1m== Cloning reveal.js from github\e[0m"
    git clone https://github.com/hakimel/reveal.js.git
    echo -e "\e[1m== Done\e[0m"
fi

if [ -n "$1" ]; then
    echo -e "\e[1m== Copying $1\e[0m"
    cp "$1.html" reveal.js/index.html
    if [ -d assets/$1 ]; then
        echo -e "   Copying assets of $1"
        cp -r "assets/$1/." reveal.js/assets/
    fi
    echo -e "   \e[32mNow you can open ./reveal.js/index.html in a browser to view the presentation.\e[0m"
    echo -e "\e[1m== Done\e[0m"
else
    echo -e "\e[32m   To copy presentation files into reveal.js use ./make <presentation>.\e[0m"
fi
